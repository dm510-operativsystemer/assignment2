/* Prototype module for second mandatory DM510 assignment */
#ifndef __KERNEL__
#define __KERNEL__
#endif

#ifndef MODULE
#define MODULE
#endif

#include <linux/module.h>
#include <linux/init.h>
#include <linux/slab.h>	
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/errno.h>
#include <linux/types.h>
#include <linux/wait.h>
#include <linux/cdev.h>
/* #include <asm/uaccess.h> */
#include <linux/uaccess.h>
#include <linux/semaphore.h>
/* #include <asm/system.h> */
#include <asm/switch_to.h>
/* Prototypes - this would normally go in a .h file */
static int dm510_open( struct inode*, struct file* );
static int dm510_release( struct inode*, struct file* );
static ssize_t dm510_read( struct file*, char*, size_t, loff_t* );
static ssize_t dm510_write( struct file*, const char*, size_t, loff_t* );
static int dm510_fasync( int fd, struct file *filp, int mode );
long dm510_ioctl( struct file *filp, unsigned int cmd, unsigned long arg );

#define DEVICE_NAME "dm510_dev" /* Dev name as it appears in /proc/devices */
#define MAJOR_NUMBER 255
#define MIN_MINOR_NUMBER 0
#define MAX_MINOR_NUMBER 1
#define DEVICE_COUNT 2
/* end of what really should have been in a .h file */

#define BUFFER_SIZE_INIT 4096 /* 1 page of memory */
static int bufferSize = BUFFER_SIZE_INIT;

#define MAX_READERS_INIT -1 /* -1 = no limit */
static int maxReaders = MAX_READERS_INIT;

/* IOCTL stuff */

#define CH_BUFF _IOW( MAGIC_NUMBER, 1, int )
#define RD_AMNT _IOW( MAGIC_NUMBER, 2, int )
#define IOCTL_MAX_CMD 2
#define MAGIC_NUMBER '*'


//buffer struct for implementing the 2 buffers
typedef struct _dm510_buffer dm510_buffer;
struct _dm510_buffer {
	char *begin, *end;                	/* begin of buf, end of buf */
    char *rp, *wp;                     	/* where to read, where to write */
    //struct mutex mutex;                	/* mutual exclusion semaphore */
};

dm510_buffer *dm510_buffer0;
dm510_buffer *dm510_buffer1;

//typedef for convenience
typedef struct _dm510_struct dm510_struct;
struct _dm510_struct {
    dm510_buffer *buffer0;              /* buffer 0 */
	dm510_buffer *buffer1;				/* buffer 1 */
    int nreaders;             			/* number of openings for r */
	int currentWriter;					/* the device with current write permission, -1 if no one currently wants permission */
    struct fasync_struct *async_queue;  /* asynchronous readers */
    struct mutex mutex;                 /* mutual exclusion semaphore */
    struct cdev cdev;                   /* Char device structure */
};

static dm510_struct *dm510_devices;


/* file operations struct */
static struct file_operations dm510_fops = {
	.owner   = THIS_MODULE,
	.read    = dm510_read,
	.write   = dm510_write,
	.open    = dm510_open,
	.release = dm510_release,
    .fasync  = dm510_fasync,
    .unlocked_ioctl   = dm510_ioctl
};

/* Wait queue split */
wait_queue_head_t inq0, outq0, inq1, outq1;

/* forward declarations */
static void dm510_setup_cdev( dm510_struct *dev, dev_t first, int index );
static int spacefree( dm510_struct *dev, int id );
static int dm510_getwritespace( dm510_struct *dev, struct file *filp );
void dm510_cleanup_module( void );

/* Set up a cdev entry. */
static void dm510_setup_cdev(dm510_struct *dev, dev_t first, int index) {
	int err, devno = first + index;
    
	cdev_init(&dev->cdev, &dm510_fops);
	dev->cdev.owner = THIS_MODULE;
	err = cdev_add (&dev->cdev, devno, 1);
	/* Fail gracefully if need be */
	if (err)
		printk(KERN_NOTICE "Error %d adding dm510%d", err, index);
}

/* called when module is loaded */
int dm510_init_module( void ) {

	/* initialization code belongs here */

    /* check for character devices */
    int check;
    dev_t first = MKDEV(MAJOR_NUMBER, MIN_MINOR_NUMBER);
    check = register_chrdev_region(first, DEVICE_COUNT, DEVICE_NAME);
	
    /* If we receive an error we handle it */
    if(check < 0) {
	    printk(KERN_NOTICE "Unable to get dm510_dev region, error %d\n", check);
		return 0;
	}

    dm510_devices = kzalloc(DEVICE_COUNT * sizeof(dm510_struct), GFP_KERNEL);
    if(!dm510_devices){
        printk(KERN_NOTICE "Unable to allocate memory for character devices, freeing memory.");
        kfree(dm510_devices);
        unregister_chrdev_region(first, DEVICE_COUNT);
        return 0;
    }

    //wipe memory clean af
    //memset(dm510_devices, 0, DEVICE_COUNT * sizeof(dm510_struct));

	dm510_devices->currentWriter = -1;

	dm510_buffer0 = kzalloc(sizeof(dm510_buffer), GFP_KERNEL);
	if(!dm510_buffer0) {
		printk(KERN_NOTICE "Unable to allocate memory for struct buffer0");
		goto fail;
	}

	dm510_buffer1 = kzalloc(sizeof(dm510_buffer), GFP_KERNEL);
	if(!dm510_buffer1) {
		printk(KERN_NOTICE "Unable to allocate memory for struct buffer1");
		goto fail;
	}

	/* allocate the buffers in the structs */
	dm510_buffer0->begin = kzalloc(bufferSize, GFP_KERNEL); //kmalloc returns a pointer to the beginning of the allocated memory
	if(!dm510_buffer0->begin) {
		printk(KERN_NOTICE "Unable to allocate memory for buffer0");
		goto fail;
	}
	dm510_buffer1->begin = kzalloc(bufferSize, GFP_KERNEL); //kmalloc returns a pointer to the beginning of the allocated memory
	if(!dm510_buffer1->begin) {
		printk(KERN_NOTICE "Unable to allocate memory for buffer1");
		goto fail;
	}
		
    //initialize dev_0
    init_waitqueue_head(&(inq0));
	init_waitqueue_head(&(outq0));
	mutex_init(&dm510_devices[0].mutex);
    dm510_setup_cdev(dm510_devices, first, 0);
	dm510_devices[0].buffer0 = dm510_buffer0;
	dm510_devices[0].buffer1 = dm510_buffer1;

    //initialize dev_1
    init_waitqueue_head(&(inq1));
	init_waitqueue_head(&(outq1));
	mutex_init(&dm510_devices[1].mutex);
	dm510_setup_cdev(dm510_devices + 1, first, 1);
	dm510_devices[1].buffer0 = dm510_buffer0;
	dm510_devices[1].buffer1 = dm510_buffer1;

	//init buffer size
	dm510_devices->buffer0->end = dm510_devices->buffer0->begin + bufferSize;
	dm510_devices->buffer1->end = dm510_devices->buffer1->begin + bufferSize;

	//dev_0: rp = 0, wp = 1
	dm510_devices->buffer0->wp = dm510_devices->buffer1->begin;
	dm510_devices->buffer0->rp = dm510_devices->buffer0->begin;
	//dev_1: rp = 1, wp = 0
	dm510_devices->buffer1->wp = dm510_devices->buffer0->begin;
	dm510_devices->buffer1->rp = dm510_devices->buffer1->begin;

	printk(KERN_INFO "DM510: Hello from your device!\n");

	return 0; /* scull returns number of devices on success, but we return 0 here to avoid the passive aggressive kernel messages on load */

	fail:
	printk(KERN_NOTICE "Error occured during initialization, freeing memory");
	dm510_cleanup_module();
	return -EAGAIN;
}

/* Called when module is unloaded */
void dm510_cleanup_module( void ) {

	/* clean up code belongs here */

    dev_t first = MKDEV(MAJOR_NUMBER, MIN_MINOR_NUMBER);

	if (dm510_buffer0) {
		if(dm510_buffer0->begin) {
			kfree(dm510_buffer0->begin);
			dm510_buffer0->begin = NULL; 
		}
		kfree(dm510_buffer0);
		dm510_buffer0 = NULL; 
	}
	if (dm510_buffer1) {
		if(dm510_buffer1->begin) {
			kfree(dm510_buffer1->begin);
			dm510_buffer1->begin = NULL; 
		}
		kfree(dm510_buffer1);
		dm510_buffer1 = NULL;
	}

	//check whether the struct has already been freed. 
	if (dm510_devices) {
		//delete cdev references and free device buffer memory
		cdev_del(&dm510_devices[0].cdev);
		cdev_del(&dm510_devices[1].cdev);

		//free devices and unregister region
		kfree(dm510_devices);
		unregister_chrdev_region(first, DEVICE_COUNT);

		//release the pointer
		dm510_devices = NULL; /* pedantic */
	}
	
	printk(KERN_INFO "DM510: Module unloaded.\n");
}

/* Called when a process tries to open the device file */
static int dm510_open( struct inode *inode, struct file *filp ) {
	
	/* device claiming code belongs here */

    dm510_struct *dev;

	dev = container_of(inode->i_cdev, dm510_struct, cdev);
	filp->private_data = dev;

	if (mutex_lock_interruptible(&dev->mutex))
		return -ERESTARTSYS;

	/* use f_mode,not  f_flags: it's cleaner (fs/open.c tells why) */
	if (filp->f_mode & FMODE_READ) {
		if (dev->nreaders == maxReaders) {
				printk( KERN_NOTICE "Error: at max readers already" );
				return -EACCES;
		}
		dev->nreaders++;
	}

	/* check to make sure only one writer can write at a time */
	if(MINOR(dev->cdev.dev) == 0) { 
		if ( filp->f_mode & FMODE_WRITE ) {
			//if open, give dev_0 access
			if(dev->currentWriter == -1) { 
				dev->currentWriter = 0;
			}
		}
	} else {
		if ( filp->f_mode & FMODE_WRITE ) {
			//if open, give dev_1 access
			if(dev->currentWriter == -1) {
				dev->currentWriter = 1;
			}
		}
	}

	mutex_unlock(&dev->mutex);
	
	return nonseekable_open(inode, filp);
}

/* helper function for adding or removing files asynchronously */
static int dm510_fasync(int fd, struct file *filp, int mode) {
	dm510_struct *dev = filp->private_data;
	return fasync_helper(fd, filp, mode, &dev->async_queue);
}

/* Called when a process closes the device file. */
static int dm510_release( struct inode *inode, struct file *filp ) {

	/* device release code belongs here */

    dm510_struct *dev = filp->private_data;

	/* remove this filp from the asynchronously notified filp's */
	dm510_fasync(  -1, filp, 0 );
	mutex_lock( &dev->mutex );

	if ( filp->f_mode & FMODE_READ )
		dev->nreaders--;
	if ( filp->f_mode & FMODE_WRITE ) {
		dev->currentWriter = -1;
	}
		
	if ( dev->nreaders == 0 && dev->currentWriter == -1 ) {
		kfree( dev->buffer0->begin );
		kfree( dev->buffer1->begin );

		dev->buffer0->begin = NULL; /* the other fields are not checked on open */
		dev->buffer1->begin = NULL; /* the other fields are not checked on open */
	}
	mutex_unlock(&dev->mutex);
	return 0;
}


/* Called when a process, which already opened the dev file, attempts to read from it. */
static ssize_t dm510_read( struct file *filp,
    char *buf,      /* The buffer to fill with data     */
    size_t count,   /* The max number of bytes to read  */
    loff_t *f_pos ) /* The offset in the file           */ {
	
	/* read code belongs here */

    dm510_struct *dev = filp->private_data;

	if (mutex_lock_interruptible(&dev->mutex))
		return -ERESTARTSYS;

	if(MINOR(dev->cdev.dev) == 0) {
		//dev_0 : rp = 0, wp = 1
		while (dev->buffer0->rp == dev->buffer1->wp) { /* nothing to read */
			mutex_unlock(&dev->mutex); /* release the lock */
			if (filp->f_flags & O_NONBLOCK)
				return -EAGAIN;

			//dev_0 : inq = 0, rp = 0, wp = 1
			if (wait_event_interruptible(inq0, (dev->buffer0->rp != dev->buffer1->wp)))
				return -ERESTARTSYS; /* signal: tell the fs layer to handle it */
			/* otherwise loop, but first reacquire the lock */
			if (mutex_lock_interruptible(&dev->mutex))
				return -ERESTARTSYS;
		}
		/* ok, data is there, return something */ //... yeah sure. let's do 'something'
		//dev_0 : inq = 0, rp = 0, wp = 1
		if (dev->buffer0->rp < dev->buffer1->wp) 
			count = min(count, (size_t)(dev->buffer1->wp - dev->buffer0->rp));
		else /* the write pointer has wrapped, return data up to dev->end */
			count = min(count, (size_t)(dev->buffer0->end - dev->buffer0->rp));
		if (copy_to_user(buf, dev->buffer0->rp, count)) {
			mutex_unlock (&dev->mutex);
			return -EFAULT;
		}
		//add count and check if wrapped (from else statement above)
		dev->buffer0->rp += count;
		if (dev->buffer0->rp == dev->buffer0->end)
			dev->buffer0->rp = dev->buffer0->begin; /* wrapped */

		mutex_unlock (&dev->mutex);
		/* finally, awake any writers and return */
		//tell other writer queue that it can do something now
		wake_up_interruptible(&outq1);
	} else {
		while (dev->buffer1->rp == dev->buffer0->wp) { /* nothing to read */
			mutex_unlock(&dev->mutex); /* release the lock */
			if (filp->f_flags & O_NONBLOCK)
				return -EAGAIN;
			
			if (wait_event_interruptible(inq1, (dev->buffer1->rp != dev->buffer0->wp)))
				return -ERESTARTSYS; /* signal: tell the fs layer to handle it */
			/* otherwise loop, but first reacquire the lock */
			if (mutex_lock_interruptible(&dev->mutex))
				return -ERESTARTSYS;
		}
		/* ok, data is there, return something */ //... yeah sure. let's do 'something'
		if ( dev->buffer1->rp < dev->buffer0->wp ) 
			count = min(count, (size_t)(dev->buffer0->wp - dev->buffer1->rp));
		else /* the write pointer has wrapped, return data up to dev->end */
			count = min(count, (size_t)(dev->buffer1->end - dev->buffer1->rp));
		if (copy_to_user(buf, dev->buffer1->rp, count)) {
			mutex_unlock (&dev->mutex);
			return -EFAULT;
		}
		//add count and check if wrapped (from else statement above)
		dev->buffer1->rp += count;
		if (dev->buffer1->rp == dev->buffer1->end)
			dev->buffer1->rp = dev->buffer1->begin; /* wrapped */

		mutex_unlock (&dev->mutex);
		/* finally, awake any writers and return */
		//tell other writer queue that it can do something now
		wake_up_interruptible(&outq0);
	}

	return count;

}

/* How much space is free? */
static int spacefree(dm510_struct *dev, int id) {
	if(id == 0) {
		//dev_0: rp = 0, wp = 1
		if (dev->buffer0->rp == dev->buffer1->wp) //all space is free
			return bufferSize - 1;
		return ((dev->buffer0->rp + bufferSize - dev->buffer1->wp) % bufferSize) - 1; //some space is free, mod operator reverses the outcome of subtraction
	} else {
		//dev_1: rp = 1, wp = 0
		if (dev->buffer1->rp == dev->buffer0->wp) //all space is free
			return bufferSize - 1;
		return ((dev->buffer1->rp + bufferSize - dev->buffer0->wp) % bufferSize) - 1; //some space is free, mod operator reverses the outcome of subtraction
	}
}

/* Wait for space for writing; caller must hold device semaphore.  On
 * error the semaphore will be released before returning. */
static int dm510_getwritespace(dm510_struct *dev, struct file *filp) {

	int id = MINOR(dev->cdev.dev); 

	while ( spacefree( dev, id ) == 0 ) { /* checks if current device write-buffer is full */
		DEFINE_WAIT(wait);
		mutex_unlock(&dev->mutex);
		if (filp->f_flags & O_NONBLOCK)
			return -EAGAIN;

		if(id == 0) {
			//dev_0: outq0
			prepare_to_wait(&outq0, &wait, TASK_INTERRUPTIBLE);
			if (spacefree(dev, id) == 0)
				schedule();
			finish_wait(&outq0, &wait);
		} else {
			//dev_1: outq1
			prepare_to_wait(&outq1, &wait, TASK_INTERRUPTIBLE);
			if (spacefree(dev, id) == 0)
				schedule();
			finish_wait(&outq1, &wait);
		}	
		
		if (signal_pending(current))
			return -ERESTARTSYS; /* signal: tell the fs layer to handle it */
		if (mutex_lock_interruptible(&dev->mutex))
			return -ERESTARTSYS;
	}

	return 0;
}	

/* Called when a process writes to dev file */
static ssize_t dm510_write( struct file *filp,
    const char *buf,	/* The buffer to get data from      */
    size_t count,   	/* The max number of bytes to write */
    loff_t *f_pos )  	/* The offset in the file           */ {

	/* write code belongs here */	

    dm510_struct *dev = filp->private_data;
	int result;

	/* first check permission to write */
	if(dev->currentWriter == -1) {
		return -EPERM;
	}

	if (mutex_lock_interruptible(&dev->mutex))
		return -ERESTARTSYS;

	/* Make sure there's space to write */
	result = dm510_getwritespace(dev, filp);
	if (result)
		return result; /* dm510_getwritespace called mutex_unlock(&dev->mutex) */

	/* ok, space is there, accept something */
	count = min(count, (size_t)spacefree(dev, MINOR(dev->cdev.dev)));
	if(MINOR(dev->cdev.dev) == 0) {
		//dev_0: rp = 0, wp = 1 
		if (dev->buffer0->wp >= dev->buffer1->rp)
			count = min(count, (size_t)(dev->buffer1->end - dev->buffer0->wp)); /* to end-of-buf */
		else /* the write pointer has wrapped, fill up to rp-1 */
			count = min(count, (size_t)(dev->buffer1->rp - dev->buffer0->wp - 1));
		
		if (copy_from_user(dev->buffer0->wp, buf, count)) {
			mutex_unlock (&dev->mutex);
			return -EFAULT;
		}
		dev->buffer0->wp += count;
		if (dev->buffer0->wp == dev->buffer1->end)
			dev->buffer0->wp = dev->buffer1->begin; /* wrapped */
		mutex_unlock(&dev->mutex);
		/* finally, awake any reader */
		wake_up_interruptible(&inq1);  /* blocked in read() and select() */
	} else {
		//dev_1: rp = 1, wp = 0
		if (dev->buffer1->wp >= dev->buffer0->rp)
			count = min(count, (size_t)(dev->buffer0->end - dev->buffer1->wp)); /* to end-of-buf */
		else /* the write pointer has wrapped, fill up to rp-1 */
			count = min(count, (size_t)(dev->buffer0->rp - dev->buffer1->wp - 1));
		
		if (copy_from_user(dev->buffer1->wp, buf, count)) {
			mutex_unlock (&dev->mutex);
			return -EFAULT;
		}
		dev->buffer1->wp += count;
		if (dev->buffer1->wp == dev->buffer0->end)
			dev->buffer1->wp = dev->buffer0->begin; /* wrapped */
		mutex_unlock(&dev->mutex);
		/* finally, awake any reader */
		wake_up_interruptible(&inq0);  /* blocked in read() and select() */
	}

	/* and signal asynchronous readers, explained late in chapter 5 */
	if (dev->async_queue)
		kill_fasync(&dev->async_queue, SIGIO, POLL_IN);
	return count;

	//return 0; //return number of bytes written
}

/* called by system call icotl */ 
long dm510_ioctl( 
    struct file *filp, 
    unsigned int cmd,   /* command passed from the user */
    unsigned long arg ) /* argument of the command */ {

	/* ioctl code belongs here */

	int err = 0, ret = 0;

    /* don't even decode wrong cmds: better returning  ENOTTY than EFAULT */
    if( _IOC_TYPE ( cmd ) != MAGIC_NUMBER ) return -ENOTTY;
    if( _IOC_NR ( cmd ) > IOCTL_MAX_CMD ) return -ENOTTY;

    /*
     * the type is a bitmask, and VERIFY_WRITE catches R/W
     * transfers. Note that the type is user-oriented, while
     * verify_area is kernel-oriented, so the concept of "read" and
     * "write" is reversed
     */
    if( _IOC_DIR( cmd ) & _IOC_READ )
        err = !access_ok( ( void __user * )arg, _IOC_SIZE( cmd ) );
    else if( _IOC_DIR( cmd ) & _IOC_WRITE )
        err =  !access_ok( ( void __user * )arg, _IOC_SIZE( cmd ) );
    if( err )
        return -EFAULT;

	switch( cmd ) {

		// Change buffer size. 
		// Doesn't accept zero or negative numbers. Resets to default if that is the case.
		case CH_BUFF: 
			ret = __get_user(bufferSize, ( int __user * )arg);
			//resize the buffer
			if(bufferSize <= 0) { //in case someone tries to set the buffer to 0 or less
				bufferSize = BUFFER_SIZE_INIT;
				printk( KERN_INFO "Setting buffer to <= 0 not permitted. Setting buffer to %d instead", BUFFER_SIZE_INIT );
			}
			if(!(bufferSize == (dm510_buffer0->end - dm510_buffer0->begin))) {
				kfree( dm510_buffer0->begin );
				kfree( dm510_buffer1->begin );
				dm510_buffer0->begin = kzalloc(bufferSize, GFP_KERNEL);
				if( !dm510_buffer0->begin ) {
					printk(KERN_NOTICE "Unable to reallocate memory for buffer0->begin");
					return -EINVAL; //EINVAL since if kmalloc fails the most probable culprit is the user arg
				}
				dm510_buffer1->begin = kzalloc(bufferSize, GFP_KERNEL);
				if( !dm510_buffer1->begin ) {
					printk(KERN_NOTICE "Unable to reallocate memory for buffer1->begin");
					return -EINVAL; //EINVAL since if kmalloc fails the most probable culprit is the user arg
				}
				dm510_buffer0->end = dm510_buffer0->begin + bufferSize;
				dm510_buffer1->end = dm510_buffer1->begin + bufferSize;

				dm510_devices->buffer0->wp = dm510_devices->buffer1->begin;
				dm510_devices->buffer0->rp = dm510_devices->buffer0->begin;

				dm510_devices->buffer1->wp = dm510_devices->buffer0->begin;
				dm510_devices->buffer1->rp = dm510_devices->buffer1->begin;

				printk( KERN_INFO "\nBuffer resized to %d", ( dm510_buffer0->end - dm510_buffer0->begin ) );
			} else {
				printk( KERN_INFO "\nBuffer already has size %d", bufferSize );
			}
			break;

		// Change amount of allowed readers. 
		// Shouldn't need further implementation, but isn't safeguarded against malicious attempts.
		case RD_AMNT: 
			ret = __get_user(maxReaders, ( int __user * )arg);
			printk(KERN_INFO "\nMax Readers have been set to %d", maxReaders);
			break;

		default:
			break;
	}

	printk(KERN_INFO "DM510: ioctl called.\n");
	return ret;
}

module_init( dm510_init_module );
module_exit( dm510_cleanup_module );

MODULE_AUTHOR( "...Nicolai Egede Petersen, Stallone Nobert" );
MODULE_LICENSE( "GPL" );