#include <sys/ioctl.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <linux/errno.h>

/* IOCTL stuff */
#define CH_BUFF _IOW( MAGIC_NUMBER, 1, int )
#define RD_AMNT _IOW( MAGIC_NUMBER, 2, int )
#define IOCTL_MAX_CMD 2
#define MAGIC_NUMBER '*'

#define BUFFER_SIZE_INIT 4096
#define MAX_READERS_INIT -1

int main(int argc, char *argv[]) {

    //for storing contemporary returns from ioctl calls
    int result; 

    // ioctl call needs an open file pointer to a device
    int filp; 
    filp = open("/dev/dm510-0", O_RDWR);

    //CH_BUFF test:
    printf("\nNow testing resizing of buffer:\n");
    fflush(stdout);
    int bufferResize = 0;
    printf("\nValue to test: %d : \n", bufferResize);
    fflush(stdout);
    result = ioctl(filp, CH_BUFF, &bufferResize);
    printf("Error code: %s\n", strerror(abs(result)));
    fflush(stdout);
    result = 0;

    bufferResize = (BUFFER_SIZE_INIT * 2);
    printf("\nValue to test: %d : \n", bufferResize);
    fflush(stdout);
    result = ioctl(filp, CH_BUFF, &bufferResize);
    printf("Error code: %s\n", strerror(abs(result)));
    fflush(stdout);
    result = 0;

    bufferResize = -1;
    printf("\nValue to test: %d : \n", bufferResize);
    fflush(stdout);
    result = ioctl(filp, CH_BUFF, &bufferResize);
    printf("Error code: %s\n", strerror(abs(result)));
    fflush(stdout);
    result = 0;

    bufferResize = (BUFFER_SIZE_INIT / 2);
    printf("\nValue to test: %d : \n", bufferResize);
    fflush(stdout);
    result = ioctl(filp, CH_BUFF, &bufferResize);
    printf("Error code: %s\n", strerror(abs(result)));
    fflush(stdout);
    result = 0;


    //RD_AMNT test:
    printf("\nNow testing resizing of max amount of readers:\n");
    fflush(stdout);
    int readerAmount = 0;
    printf("\nValue to test: %d : \n", readerAmount);
    fflush(stdout);
    result = ioctl(filp, RD_AMNT, &readerAmount);
    printf("Error code: %s\n", strerror(abs(result)));
    fflush(stdout);
    result = 0;

    readerAmount = 10000;
    printf("\nValue to test: %d : \n", readerAmount);
    fflush(stdout);
    result = ioctl(filp, RD_AMNT, &readerAmount);
    printf("Error code: %s\n", strerror(abs(result)));
    fflush(stdout);
    result = 0;

    readerAmount = -1;
    printf("\nValue to test: %d : \n", readerAmount);
    fflush(stdout);
    result = ioctl(filp, RD_AMNT, &readerAmount);
    printf("Error code: %s\n", strerror(abs(result)));
    fflush(stdout);
    result = 0;

    readerAmount = 10;
    printf("\nValue to test: %d : \n", readerAmount);
    fflush(stdout);
    result = ioctl(filp, RD_AMNT, &readerAmount);
    printf("Error code: %s\n", strerror(abs(result)));
    fflush(stdout);
    result = 0;

    //exit
    close(filp);
    return 0;
}